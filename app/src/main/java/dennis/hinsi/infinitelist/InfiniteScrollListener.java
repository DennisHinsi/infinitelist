/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.infinitelist;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class InfiniteScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private final int thresholdCount;
    private int thresholdBorder;

    public InfiniteScrollListener(
        @NonNull final LinearLayoutManager linearLayoutManager,
        final int visibleThreshold,
        final int thresholdCount
    ) {
        layoutManager = linearLayoutManager;
        this.thresholdCount = thresholdCount;
        thresholdBorder = visibleThreshold + thresholdCount;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (layoutManager.findLastVisibleItemPosition() == thresholdBorder - 1) {
            load(thresholdBorder, thresholdBorder + thresholdCount, recyclerView);
            thresholdBorder += thresholdCount;
        }
    }

    public abstract void load(
        final int startIndex,
        final int endIndex,
        final RecyclerView recyclerView
    );
}
