/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.infinitelist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class InfiniteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> contentList = new ArrayList<>();
    private boolean isLoading = false;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1)
            return new InfiniteVH(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_view, parent, false)
            );
        else
            return new InfiniteProgressVH(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_loading, parent, false)
            );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof InfiniteVH) {
            ((InfiniteVH) holder).bind(contentList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoading && contentList.get(position) == null)
            return 0;
        else
            return 1;
    }

    public void addToList(List<String> list) {
        contentList.addAll(list);
    }

    public void showLoading() {
        isLoading = true;
        contentList.add(null);
        notifyItemInserted(contentList.size() - 1);
    }

    public void hideLoading() {
        isLoading = false;
        int position = contentList.size() - 1;
        contentList.remove(position);
        notifyItemRemoved(position);
    }
}
