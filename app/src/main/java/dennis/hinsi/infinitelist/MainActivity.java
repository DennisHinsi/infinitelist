/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.infinitelist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // FOR USER VISIBLE ITEM COUNT
    private final int visibleCount = 40;

    // ITEMS PRELOAD COUNT
    private final int thresholdCount = 15;

    private InfiniteAdapter infiniteAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> content = new ArrayList<>();
    private RecyclerView contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupContentList();
        setupRecyclerView();
        setupOnScrollListener();
    }

    private void setupContentList() {
        for (int i = 0; i < 400; i++)
            content.add("item at index (" + i + ")");
    }

    private void setupRecyclerView() {
        infiniteAdapter = new InfiniteAdapter();
        infiniteAdapter.addToList(content.subList(0, visibleCount + thresholdCount));

        layoutManager = new LinearLayoutManager(getApplicationContext());

        contentView = findViewById(R.id.recycler_view);
        contentView.setLayoutManager(layoutManager);
        contentView.setAdapter(infiniteAdapter);
    }

    private void setupOnScrollListener() {
        InfiniteScrollListener scrollListener = new InfiniteScrollListener(layoutManager, visibleCount, thresholdCount) {
            @Override
            public void load(final int startIndex, final int endIndex, final RecyclerView recyclerView) {
                // SHOW LIST ITEM WITH PROGRESSBAR
                infiniteAdapter.showLoading();

                recyclerView.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // HIDE LIST ITEM WITH PROGRESSBAR
                        infiniteAdapter.hideLoading();

                        // SET NEW LOADED CONTENT
                        if (endIndex < content.size()) {
                            infiniteAdapter.addToList(content.subList(startIndex, endIndex));
                        } else {
                            infiniteAdapter.addToList(content.subList(startIndex, content.size()));
                        }
                        infiniteAdapter.notifyDataSetChanged();
                    }

                }, 1000);
            }
        };
        contentView.addOnScrollListener(scrollListener);
    }
}
